# Arduino Racal Cougar PRM4515L Programmer

This is an experimental Arduino sketch allowing you to program the Racal Cougar PRM4515L (Lowband VHF, 66 - 88 Mz).

## Pin usage

By default this sketch uses 3 Arduino pins:

Pin D3 - Connected to Pin F of Racal Cougar 7 pole audio/fill connector
Pin D5 - Green led
Pin D6 - Red led

## More information

[About the Racal Cougar Program Protocol](http://pa5dof.blogspot.nl/2017/01/program-racal-cougar-part-2.html)

[About this sketch and how to program](http://pa5dof.blogspot.nl/2017/02/program-racal-cougar-part-3.html)

[How to hook up the Arduino to the Racal Cougar]()

## IMPORTANT

You are using this sketch at your own risk

## CHANGELOG

0.0.4	15-10-2017	Changed EEProm channel data read/ write logic so it no longers loads garbage on a fresh Arduino (when channels haven't been stored before)
