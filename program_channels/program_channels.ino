/*
  Arduino Racal Cougar Programmer

  This skettch allows you to program the channel transmit and receive frequencies as well
  as enable/disable the 150 hz transmitted pilot tone. 

  It does so by using "PWM" like modulation on digital pin 3

  Use the serial monitor (or any other terminal emulator) set to 19200 baud and line ending set
  to 'both NL & CR' to access the programming interface. 
  
  WARNING

  Pin F of the Racal Cougar E.C.U / Audio connector communicates using PWM data of about
  8 Volt. Since the Arduino can't handle this voltage level some kind of level shifting to
  5 volt is required (i.e. using a 2N7000 mosfet ) is required.

  @version 0.0.4

  73' Dave PA5DOF 
 */
#include <math.h>
#include <EEPROM.h>
#include <avr/pgmspace.h>

// Version number
#define VERSION "0.0.4"

// Pin number connected to the 'F' Pin of the Racal Cougar data cable
// this should be a pin which supports an interrupt. (pins 2 and 3 on an Nano)
#define FPin 3

// Pins numbers connected to the bicolor led (green/red)
#define GPin 5
#define RPin 6

// Min and max programmable frequencies
const unsigned long model_freq_l[] PROGMEM = {66000000, 87987500};
const unsigned long model_freq_h[] PROGMEM = {132000000, 173987500}; 
const unsigned long* const model_frequencies[] PROGMEM = {model_freq_l, model_freq_h};

// model names
const char model_name_l[] PROGMEM = {"PRM 4515 L"};
const char model_name_h[] PROGMEM = {"PRM 4515 H"};
const char* const model_names[] PROGMEM = {model_name_l, model_name_h};

const uint8_t MODEL_L = 0;
const uint8_t MODEL_H = 1;

uint8_t selectedModel = MODEL_L;

unsigned long freqMin;
unsigned long freqMax;


/*
   The Volatile qualifier instructs the compiler to
   "store" these variables in RAM instead of storage registers
   The reason for this is that the storage rigisters can be inaccurate when
   accessing them from within an interrupt service routine
*/
volatile int            pwm_value   = 0;          // the 'width' of a PWM pulse in uSec
volatile unsigned long  prev_time   = 0;          // Helper to determin the pwm value
volatile int            bitCounter  = 8;          // Bit number comming in
volatile byte           myByte      = 0;          // The byte that is getting filled with incomming bits
volatile int            byteIndex   = 0;          // Index of the byte being stored in the data array
volatile byte           dataReceived[7];          // Array of bytes which will be displayed after the last bit has come in.

/*
  Used for serial communication (i.e. console) 
*/
// Used to handle serial commands
String                  inputString     = "";     // stores the text entered using the console
bool                    stringComplete  = false;  // Flag set when enter is given in console
int                     selectedChannel = 0;      // Current selected channel number
char                    selectedCommand;          // Stores the current selected command

/*
   Channel storage

   A composite data type used to store information about a channel.
   .freqRx & .freqTx are used to store the frequency in Hz / 10
*/
struct channel {
  int number;
  unsigned long freqRx; 
  unsigned long freqTx;
  bool subtone = true;
};

channel                 channels[3][10];             // An array of 10 channels


/**
 * Main routine which is run once after boot / reset
 * Used to prepare the programm for running.
 */
void setup() {
  // Init the serial interface:
  Serial.begin(19200);

  // Set led pins to output
  pinMode(RPin, OUTPUT);
  pinMode(GPin, OUTPUT);

  // reserve 10 bytes for input string used to store values entered in the console
  inputString.reserve(10);

  
  
  // load channel data from eeprom
  readChannelsFromEeprom();

  // set frequencies to MODEL_L (low VHF) by default
  setFrequencyBounds(selectedModel); 

  // display help at startup
  displayHelp();
  Serial.println(F("Enter command/value or ? for help\n"));

  // flash the red and green led each for one second
  // Just a visiual indicator we're ready to go.
  digitalWrite(RPin, HIGH);
  delay(1000);
  digitalWrite(RPin, LOW);
  digitalWrite(GPin, HIGH);
  delay(1000);
  digitalWrite(GPin, LOW);
  
}

/**
 * Main loop
 */
void loop() {
  // Process console input when enter has been received
  if (stringComplete) {
    doCommand();
  }
}

/**
  Write a channel program command to the Racal
  
  Returns true on success, when the Racal Cougar echoed the command correctly (with bit 7 set to 0)

  @param int           channel Channel number (0..9)
  @param bool          rx      Set rx or tx frequency (66 - 88 Mhz)
  @param float         freq Khz Set the frequency in Khz, i.e. 70450 for 70.450 Mhz. Should be a multiple of 12.5

  @return bool
*/
bool program(int channel, bool rx, float freqKhz) {

  // 7 bytes of storage for command
  byte cmd[7];
  // add header
  cmd[0] = 0x7E;
  // set tx/rx
  cmd[1] = reverse(rx);
  // set channel nr
  cmd[1] = cmd[1] | reverse(channel) >> 4;

  // byte 3 & 4 are equal to byte 1 & 2
  cmd[2] = cmd[0];
  cmd[3] = cmd[1];

  // split the frequency in parts to be set in the cmd
  float lowKhz = fmod(freqKhz, 100);
  int highKhz = (fmod(freqKhz, 1000) - lowKhz) / 100.00;
  int mhz =  (freqKhz - (highKhz * 100) - lowKhz) / 1000.00;
  int mhz1 = fmod(mhz, 10);
  int mhz10 = (mhz - mhz1) / 10;
  int mhz100 = 0;
  
  if (mhz10 > 9) {
    mhz100 = mhz10;
    mhz10 = fmod(mhz10, 10);
    mhz100 = (mhz100 - mhz10) / 10;
  }

  // first set the low khz part / 12.5 (channel raster)
  if (lowKhz == 0) {
    cmd[4] = 0x0;
  } else if (lowKhz == 12.5) {
    cmd[4] = 0x01;
  } else if (lowKhz == 25) {
    cmd[4] = 0x04;
  } else if (lowKhz == 37.5) {
    cmd[4] = 0x05;
  } else if (lowKhz == 50) {
    cmd[4] = 0x02;
  } else if (lowKhz == 62.5) {
    cmd[4] = 0x03;
  } else if (lowKhz == 75) {
    cmd[4] = 0x06;
  } else if (lowKhz == 87.5) {
    cmd[4] = 0x07;
  }
  // shift the bits 4 positions to the left
  cmd[4] = cmd[4] << 4;
  // then add the high khz part
  cmd[4] = cmd[4] | reverse(highKhz) >> 4;

  // First n * 1 Mhz value
  cmd[5] = reverse(mhz1);
  // Then add the n * 10 Mhz value
  cmd[5] = cmd[5] | reverse(mhz10) >> 4;
  // n * 100 Mhz value
  cmd[6] = reverse(mhz100);

  // set parity bit on first byte of data bytes
  setParity(cmd[4], cmd[5], cmd[6]);

  return sendCommand(cmd, 7, true);
}

/**
 * Send the disable pilot tone command
 * This one needs to be send right before the transmit progam command is executed
 */
bool noTone() {
   // 4 bytes of storage for command
  byte cmd[4] = {0x7F, 0xB3, 0x7F, 0xB3};
  
  bool retval = sendCommand(cmd, 4, true);
  delayMicroseconds(500);
  return retval;
}

/**
 * Send the "Init" command ?
 */
void callInit() {
  // 4 bytes of storage for command
  byte cmd[4] = {0x7F, 0x3B, 0x7F, 0x3B};

 
  // send the command to the racal
  sendCommand(cmd, 4, false);
  delayMicroseconds(500);

}

/**
 * Send the "Start program" command ?
 */
bool callStart() {
  // 4 bytes of storage for command
  byte cmd[4] = {0x7F, 0x8B, 0x7F, 0x8B};
  return sendCommand(cmd, 4, true);
}

/**
 * Send the "End Programm" command ?
 */
bool callEnd() {
  // 4 bytes of storage for command
  byte cmd[4] = {0x7F, 0x03, 0x7F, 0x03};

  // send the command to the racal
  return sendCommand(cmd, 4, true);
}

/**
   Reverse the order of the bits in a byte

   @param byte  data

   @return byte
*/
byte reverse(byte data) {

  byte retVal = 0;

  for (int lusa = 0; lusa < 8; lusa++) {
    retVal = (retVal << 1) | ( 0x01 & ( data >> lusa ));
  }

  return retVal;
}

/**
   Flip the even parity bit on the first byte if needed

   @param unsigned long  firstByte  byRef
   @param unsigned long  secondByte
   @param unsigned long  thirdByte
*/
void setParity(byte &firstByte, byte secondByte, byte thirdByte) {

  unsigned long freqWord = 0;
  int oneCounter = 0;

  freqWord =  (firstByte * 65536);
  freqWord += (secondByte * 256); 
  freqWord += thirdByte;

  while (freqWord != 0) {
    oneCounter++;
    freqWord &= (freqWord-1);  
  }

  if ((oneCounter & 1) == 0) {
    bitSet(firstByte, 7);
  } else {
    bitClear(firstByte, 7);
  }
}

/**
   Switch to receive mode

   Starts by having a ISR set to the falling (from high to low) event of the F pin
   which will record the current running time (prev_time) and changes the event from falling to rising
   the rising ISR will check the difference between the stored and current running time and determin if
   a 1 or a 0 is received.

   When data has been received the return value will be true.

   @return bool
*/
bool receive() {
  // reset used variables
  pwm_value = 0;
  prev_time = 0;
  bitCounter = 8;
  myByte = 0;
  memset((char *) dataReceived, 0, sizeof(dataReceived));;
  byteIndex = 0;

  // Set F pin to input with build in pullup
  pinMode(FPin, INPUT_PULLUP);

  // Start with attaching a falling signal (1 to 0) event to
  // trigger an incomming bit
  attachInterrupt(1, falling, FALLING);

  // The current running time in micro seconds
  unsigned long tNow = micros();

  // Wait till we have a response or no data for 200 ms
  while (prev_time == 0 && tNow > (micros() - 200000)) {}

  // If prev_time has been set it means we received something
  if (prev_time > 0) {

    // When there has been no change for 1 mSec
    while (prev_time > (micros() - 1000)) {}

    // for incomplete bytes (i.e. a 52 bit stream)
    // add it to the data array
    if (bitCounter < 8) {
      bitCounter = 8;
      dataReceived[byteIndex] = myByte;
      byteIndex++;
    }
    return true;
  } else {
    return false;
  }
}

/**
   validate if command was received succesfully

   This is done by comparing the received header byte with the send header byte,
   the Racal Cougar echos the send command with the 2nd bit of the header set to 0

   @param byte req    Send header byte
   @param byte resp   Received header byte

   @return bool
*/
bool validateCommand(byte req, byte resp) {
  // change the 2nd bit of the request header to 0
  bitClear(req, 1);
  return (req == resp);
}

bool sendCommand(byte data[], int arrSize, bool echo) {

  // disbale led
  digitalWrite(GPin,LOW);
  digitalWrite(RPin,LOW);
  
  // disable interrupt while sending data
  detachInterrupt(digitalPinToInterrupt(FPin));
  // Set the F pin to output
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, HIGH);
  // wait 2 ms
  delay(2);
  // send the command bytes
  sendWord(data, arrSize);

  if (! echo) {
    return true;
  }

  // Switch to receive and wait for data received from the Racal Cougar
  if (receive()) {
    delay(50);
    // if we have received data, validate the response and return the result
    bool result = validateCommand(data[2], dataReceived[2]);

    // dependening on the result light up the red or green led
    result == true ? digitalWrite(GPin,HIGH):digitalWrite(RPin,HIGH);

    return result;
  }

  // No data received
  delay(50);
  return false;

}
/**
 * Send the array of command bytes to the F pin using sendByte and pulseOut functions
 * 
 * @param byte  data[]  array of command bytes
 * @param int   arrSize number of bytes to send (32 or 52 bits commands)
 */
void sendWord(byte data[], int arrSize) {

  int bitCount = 8;

  for (int lusa = 0; lusa < arrSize; lusa++) {
    if (lusa == 6) {
      bitCount = 4;
    }

    sendByte(data[lusa], bitCount);
  }
}

/**
 * Send a Byte to the F pin (using pulseOut function)
 * 
 * @param byte  value     The byte to send
 * @param int   bitcount
 */
void sendByte(byte value, int bitcount) {
  for (byte mask = 0x80; mask; mask >>= 1) { //iterate through bit mask
    if (value & mask) { // if bitwise AND resolves to true
      pulseOut(FPin, HIGH); // send 1
    }
    else { //if bitwise and resolves to false
      pulseOut(FPin, LOW); // send 0
    }
  }
}

/**
 * Send a bit to the F pin
 * 
 * @param int   pin   Pin number for pin F (3)
 * @param byte  value but value to send
 */
void pulseOut(int pin, byte value) {
  digitalWrite(FPin, LOW);
  if (value) {
    delayMicroseconds(180);
    digitalWrite(FPin, HIGH);
    delayMicroseconds(60);
  } else {
    delayMicroseconds(70);
    digitalWrite(FPin, HIGH);
    delayMicroseconds(160);
  }

}

/**
   Falling ISR

   This is the start of a pulse event, stores the current running time
   and changes the interrupt from a falling to a rising event
*/
void falling() {
  prev_time = micros();
  attachInterrupt(1, rising, RISING);
}

/**
   Rising ISR

   This is the end of a pulse event, it measures the time between falling and rising
   to determine the bit value being send.
*/
void rising() {

  // reset the interrupt to a falling state
  attachInterrupt(1, falling, FALLING);

  // Contains the current running time in uSec
  unsigned long mTime = micros();

  // calculate the pulse width
  pwm_value = mTime - prev_time;

  prev_time = mTime;

  // Set the bit value into the byte
  // 0 = 75 uSec
  // 1 = 175 uSec
  if ( pwm_value > 100) {
    myByte =   myByte | (1 << (bitCounter - 1)) ;
  }

  // lower the bitcounter and store the byte in the data array when the last
  // bit of the byte has been set.
  bitCounter--;
  if (bitCounter == 0) {
    bitCounter = 8;
    dataReceived[byteIndex] = myByte;
    byteIndex++;
    myByte = 0;
  }
}

/**
   Print a Line to the console
*/
void printLine() {
  Serial.println(F("-------------------------------------------------------------------------"));
}

/**
   Printout the current channel settings
*/
void listChannels(uint8_t model) {
  Serial.println(F("Channels:"));
  printLine();
  Serial.println(F("# RX           TX           Tone"));
  for (int lusa = 0; lusa < 10; lusa++) {
    displayChannel(model, lusa, false);
  }
  printLine();
}

/**
   Display channel properties

   @param uint8_t   model   Model type 0..3
   @param uint8_t   channel Channel number 0...9
   @param bool  header  Should header (line + header text)
*/
void displayChannel(uint8_t model, uint8_t channel, bool header) {
  if (header) {
    printLine();
    Serial.println(F("# RX           TX           Tone"));
  }
  char buffer[32];
  char subtone = (channels[model][channel].subtone ? 'Y' : 'N');
  sprintf(buffer, "%d %05ld.%02ld Khz %05ld.%02ld Khz %c", channel, channels[model][channel].freqRx / 100, channels[model][channel].freqRx % 100, channels[model][channel].freqTx / 100, channels[model][channel].freqTx % 100, subtone);
  Serial.println(buffer);
  if (header) {
    printLine();
  }
}


void displaySelectModel() {
  Serial.println(F("Select Model"));
  printLine();
  Serial.println(F("0\tPRM 4515 L\t66,000,000\t87,987,500\tMhz."));
  Serial.println(F("1\tPRM 4515 H\t132,000,000\t173,987,500\tMhz."));
  printLine();
  
}

/**
   Display the command overview
*/
void displayHelp() {
  Serial.print(F("Racal Cougar PRM 4515 Programmer version v"));
  Serial.println(VERSION);
  displaySelectedModel(selectedModel);
  printLine();
  Serial.println(F("Commands:"));
  Serial.println(F("\t?\tDisplay this help."));
  Serial.println(F("\tm\tSelect PRM 4515 model to program."));
  Serial.println();
  Serial.println(F("\tl\tDisplay list of channel settings."));
  Serial.println(F("\t0...9\tSelect channel to configure."));
  Serial.println(F("\tr\tSet RX frequency for selected channel."));
  Serial.println(F("\tt\tSet TX frequency for selected channel."));
  Serial.println(F("\ts\tEnable/Disable 150 hz subtone for selected channel."));
  Serial.println();
  Serial.println(F("\tw\tWrite channel information to programmers eeprom."));
  Serial.println(F("\tc\tClear channel information from RAM."));
  Serial.println(F("\tp\tProgram channels into set."));
  printLine();
}

/**
   Set the current selected channel

   @param uint8_t channel Channel number 0...9
*/
void selectChannel(uint8_t model, uint8_t channel) {
  selectedChannel = channel;
  selectedCommand = channel;
  Serial.print(F("Channel "));
  Serial.print(channel, DEC);
  Serial.println(F(" Selected"));

  displayChannel(model, channel, true);
}

/**
* Store current array of channels into EEProm
*/
void writeChannelsToEeprom() {

  // Address 1 5 + 0   + (chan * 11) for PRM 4515 L channel data
  // Address 2 5 + 140 + (chan * 11) for PRM 4515 H channel data
  // Address 3 5 + 280 + (chan * 11) for PRM 4515 U channel data

  uint8_t memOffset = 5;

  // identfier used in read from eeprom function to check
  // if the EEProm has been written before. This is needed to prevent reading garbage
  // when running for the first time.
  EEPROM.put(0, 'r');
  EEPROM.put(1, 'a');
  EEPROM.put(2, 'c');
  EEPROM.put(3, 'a');
  EEPROM.put(4, 'l');

  for (uint8_t lusa = 0; lusa < 3; lusa++) {
    for (int lusb = 0; lusb < 10; lusb++) {
      EEPROM.put(memOffset+ (lusa*140) + (lusb * 14), channels[lusa][lusb]);
    }
  }

  Serial.println("Saved channels to Eeprom.");
}

/**
* Read channel from EEprom and store them in channels array
*/
void readChannelsFromEeprom() {

  // Address 1 5 + 0   + (chan * 11) for PRM 4515 L channel data
  // Address 2 5 + 140 + (chan * 11) for PRM 4515 H channel data
  // Address 3 5 + 280 + (chan * 11) for PRM 4515 U channel data
  
  channel chan;
  uint8_t memOffset = 5;
  char    identifier[6];
  char    identifierChar;

  // Read first 5 bytes, this should be the identifier string reading 'racal'
  for (uint8_t lusa=0; lusa < memOffset; lusa++) {
    EEPROM.get(lusa, identifierChar);
    identifier[lusa] = identifierChar;  
  }
  identifier[5] = '\0'; // "racal" is actualy a char[6] with last element being
                        // a null terminator. We need to add this to make strcmp()
                        // work.

  Serial.println(identifier);

  // Check if identifier string matches 'racal'
  if (strcmp(identifier, "racal") != 0) {
    // no match, set default channels
    Serial.println("Default channels");
    setDefaultChannels();
  } else {
    // match, read from EEProm
    for (uint8_t lusa = 0; lusa < 3; lusa++) {
      for (uint8_t lusb = 0; lusb < 10; lusb++) {
        EEPROM.get(memOffset + (lusa*140) + (lusb * 14), chan);
        channels[lusa][lusb] = chan;
      }
    }
  }  
}

/**
 * Fill the channels array with default channel frequencies
 */
void setDefaultChannels() {

  unsigned long defaultModelKhz;
  channel       chan;
  
  for (uint8_t lusa = 0; lusa < 3; lusa++) {

    switch(lusa){
      case 0:
        defaultModelKhz = 70000;
        break;
      case 1:
        defaultModelKhz = 145000;
        break;
      case 2:
        defaultModelKhz = 430000;
        break;  
    }

    for (uint8_t lusb = 0; lusb < 10; lusb++) {
      chan.number = lusb;
      chan.freqRx = (defaultModelKhz + 250 + (lusb * 25)) * 100;
      chan.freqTx = (defaultModelKhz + 250 + (lusb * 25)) * 100;
      chan.subtone = true;
      channels[lusa][lusb] = chan;
    }

  }
  
}

/**
   Reset the values in the channels array (Not the EEProm)
*/
void clearChannels(uint8_t model) {
  for (int lusa = 0; lusa < 10; lusa++) {
    memset(&channels[model][lusa], 0, sizeof(channels[model][lusa]));
  }
  Serial.println("Cleared channels in RAM.");
}

/**
   Depending on the entered text perfrom an action
*/
void doCommand() {
  inputString.trim();

  if (inputString == "l") {
    listChannels(selectedModel);
  }else if (inputString == "m") {
    displaySelectModel();
    selectedCommand = 'm';
  } else if (inputString == "?") {
    displayHelp();
  } else if (inputString == "w") {
    writeChannelsToEeprom();
  } else if (inputString == "p") {
    if (!validateChannels(selectedModel) || !programChannels(selectedModel)) {
      Serial.println("An error occured during programming.");
    } else {
      Serial.println("OK");
    }
  } else if (inputString == "c") {
    clearChannels(selectedModel);
  } else if ( selectedCommand == 'm' && (inputString.toInt() > -1 || inputString.toInt() < 2)) {
    selectedModel = inputString.toInt();
    setFrequencyBounds(selectedModel);
    displaySelectedModel(selectedModel); 
    selectedCommand = ' ';
  } else if (inputString == "0") {  
    selectChannel(selectedModel, inputString.toInt());
  } else  if (inputString.length() == 1 && inputString.toInt() > 0 && inputString.toInt() < 10) {
    selectChannel(selectedModel, inputString.toInt());
  } else if (inputString == "t") {
    selectedCommand = 't';
    printLine();
    Serial.print(F("Enter transmit frequency for channel "));
    Serial.println(selectedChannel);
    printLine();
  } else if (inputString == "r") {
    selectedCommand = 'r';
    printLine();
    Serial.print(F("Enter receive frequency for channel "));
    Serial.println(selectedChannel);
    printLine();
  } else if (inputString == "s") {
    selectedCommand = 's';
    printLine();
    Serial.print(F("Use subtone for channel "));
    Serial.print(selectedChannel);
    Serial.println(F(" ? (y/n)"));
    printLine();
  } else {
    // values entered
    if (selectedCommand == 's' && (inputString != "y" && inputString != "n") ) {
      Serial.println(F("Error"));
    } else if (selectedCommand == 's' && ( inputString == "y" || inputString == "n")) {
      channels[selectedModel][selectedChannel].subtone = inputString == "y" ? true : false;
      displayChannel(selectedModel, selectedChannel, true);
    } else if (
      (selectedCommand == 'r' || selectedCommand == 't') &&
      (
        (inputString.toFloat() < (freqMin / 1000.00) || inputString.toFloat() > (freqMax / 1000.00)  ) ||
        fmod(inputString.toFloat(), 12.5) > 0
      )
    ) {
      Serial.println(freqMax / 1000);
      Serial.println(F("Error"));
    } else if ( selectedCommand == 'r' ) {
      Serial.println(inputString.toFloat());
      channels[selectedModel][selectedChannel].freqRx = (long) (inputString.toFloat() * 100.0);
      displayChannel(selectedModel,selectedChannel, true);
    } else if ( selectedCommand == 't' ) {
      channels[selectedModel][selectedChannel].freqTx = (long) (inputString.toFloat() * 100.0);
      displayChannel(selectedModel, selectedChannel, true);
    }
  }

  // reset entered text
  inputString = "";
  stringComplete = false;

  Serial.println(F("Enter command/value or ? for help\n"));
}

/**
   Validate if all channels are set
*/
bool validateChannels(uint8_t model) {
  for (int lusa = 0; lusa < 10; lusa++) {
    if (channels[model][lusa].freqRx < (freqMin / 10.00) || channels[model][lusa].freqRx > (freqMax / 10.00)) {
      return false;
    }
    if (channels[model][lusa].freqTx < (freqMin / 10.00) || channels[model][lusa].freqTx > (freqMax / 10.00)) {
      return false;
    }
    if (fmod((float) channels[model][lusa].freqRx , 12.5) > 0) {
      return false;
    }
    if (fmod((float) channels[model][lusa].freqTx , 12.5) > 0) {
      return false;
    }
  }

  return true;
}

/**
 * Program the channels into the PRM
 * 
 * false true on program error
 * 
 * @returns bool
 */
bool programChannels(uint8_t model) {

  callInit();
  
  if(!callStart()){
    Serial.println("start error");
    return false;
  }

  for (int lusa = 0; lusa < 10; lusa++) {

    // Disable the 150 hz pilot tone if needed (only supported for 4515L model)
    if (model == 0 && channels[model][lusa].subtone == false && ! noTone()) {
      return false;
    }
    
    // channel rx and tx frequency
    if (
      !program(lusa, false, (channels[model][lusa].freqTx / 100.00)) ||
      !program(lusa, true, (channels[model][lusa].freqRx / 100.00)) 
    ) {
      return false;
    }
  }

  if(!callEnd()){
    return false;
  }

  return true;
}

/**
 *   Called when serial data is available
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();

    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } else {
      // add it to the inputString:
      inputString += inChar;
    }
  }

}

/**
 * Set the minimal and maximal frequencies depending on selected model
 * 
 * @param uint8_t model selected model
 */
void setFrequencyBounds(uint8_t model){
  unsigned long * pointer = (unsigned long *) pgm_read_word(&(model_frequencies[model]));
  freqMin =  pgm_read_dword_near(pointer);
  freqMax =  pgm_read_dword_near(pointer + 1);
}

/**
 * Displays the currently selected model
 * 
 * @param uint8_t model selected model
 */
void displaySelectedModel(uint8_t model){
  char buffer[11];
  strcpy_P(buffer, (char*)pgm_read_word(&(model_names[model]))); 
  Serial.print(F("Selected model "));
  Serial.println(buffer); 
}

